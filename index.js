import Vue from "vue";
import bootstrapVue from "bootstrap-vue";
import "bootstrap/dist/css/bootstrap.min.css";
import "material-design-icons/iconfont/material-icons.css";
import App from "./App.vue";

Vue.config.productionTip = false;
Vue.use(bootstrapVue);

new Vue({
  render: h => h(App)
}).$mount("#app");
